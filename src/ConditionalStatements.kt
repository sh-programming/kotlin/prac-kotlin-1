fun main(args:Array<String>){

    //Relational Operators

    var first = 5
    var second = 6
    var third = 5
    println(first < second)
    println(first > second)
    println(first == second)
    println(first != second)
    println(first == third)

    //Conditional Statements
    //IF-ELSE
    println("IF ELSE")
    if(first <= second){
        println("First is less than or equal to second")
    }
    else{
        println("Second number is greater")
    }

    //IF ELSE in single line
    if(first < second) println("YES") else println("NO")

    //LOGICAL OPERATORS ARE && || and !

    //WHEN : same as switch
    println("WHEN")
    println("Enter your age : ")
    var age = readLine()!!.toInt()
    when(age){
        17 -> println(17)
        18 -> println("You are just an adult")
        19 -> println("Yep you are qualified")
        else -> println("None of the above")
    }

    //FOR LOOP
    println("FOR LOOP")
    for (item in 1..5){
        println("Hi $item")
    }

    //WHILE LOOP is same as JS

    /*DO WHILE LOOP
    do{
        ...
    }
    while(condition)
     */

}