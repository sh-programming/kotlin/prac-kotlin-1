fun main(args:Array<String>){
    //Difference between var and val

    //VAR
    println("Testing VAR")
    var myName = "prabhashankar"
    println(myName)
    //VAR can be changed
    myName = "sangeetha"
    println(myName)

    val name = "hari"
    //INVALID : name = "hello"
    //VAL cannot be changed
    println(name)
}