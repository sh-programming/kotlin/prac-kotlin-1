//Abstract Classes : They are not meant to be instantiated
//They can have a mixture of functions with or without an implementation

abstract class GraphicObject{
    fun moveTo(newX : Int, newY: Int){
        println("Moved to new x and new y")
    }
    abstract fun draw()
    abstract fun resize()
}
class Circle: GraphicObject(){
    override fun resize() {
        println("Circle resized")
    }

    override fun draw() {
        println("Circle drawn")
    }

}

fun main(args: Array<String>) {
    var circle = Circle()
    circle.draw()
    circle.moveTo(5,4)
    circle.resize()
    circle.draw()
}